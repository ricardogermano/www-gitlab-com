---
layout: job_family_page
title: "Revenue Programs"
---

The Revenue Programs job family is responsible for developing and executing effective pipeline tactics and strategies to drive growth within our sales organization.

## Responsibilities
* Identify, prioritize, and run sales programs that drive quality pipeline and ARR aligned to GitLab’s go to market (GTM) motions
* Work cross-functionally (with Portfolio Marketing, Demand Gen, Revenue Marketing, Field Enablement, Sales Strategy, and more) to orchestrate the development, execution, and operationalization of scalable and measurable quarterly sales programs that deliver results
* Own quarterly pipeline planning to ensure there is structure, scalability and transparency with clear in-quarter execution outputs
* Work comfortably with complex data sets to identify pipeline gaps and prescribe and prioritize solutions to meet business pipeline gaps
* Conduct regular program reviews with leaders, providing status updates and risk mitigation strategies
* Collaboratively define success measures and reporting frameworks for implemented programs

## Requirements
* Experience managing or designing sales programs, campaigns, and go-to-market strategies
* Proven ability to design and operationalize prescriptive sales programs and strategies
* Ability to work collaboratively with Sales & Marketing teams and effectively develop relationships with stakeholders at all levels of the organization to remove impediments that block the team’s ability to meet project objectives
* Exceptional communication and presentation skills
* Proven track record of cross-stakeholder program management and execution with operational rigor
* Skills to build, maintain, update and optimize critical dashboards in Salesforce.com and business intelligence (BI) tools for the purpose of tracking the impact of sales programs
* Strong analytical skills
* Bachelor or University Degree, or relevant work experiences

## Levels
Read more about [levels at GitLab](/handbook/hiring/vacancies/#definitions).

### Revenue Programs Specialist (Intermediate) 
The Revenue Programs Specialist (Intermediate) reports to the Manager, Revenue Programs.

#### Revenue Programs Specialist (Intermediate) Job Grade
The Revenue Programs Specialist (Intermediate) role is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Revenue Programs Specialist (Intermediate) Responsibilities
* Provide reporting, analytics, and communications support for prioritized sales programs that drive quality pipeline and ARR aligned to GitLab’s go to market (GTM) motions
* Own quarterly pipeline planning to ensure there is structure, scalability and transparency with clear in-quarter execution outputs
* Collaboratively define success measures and reporting frameworks for implemented programs

#### Revenue Programs Specialist (Intermediate) Requirements
* Strong analytical skills
* Exceptional communication and presentation skills
* Bachelor or University Degree, or relevant work experiences
* Skills to build, maintain, update and optimize critical dashboards in Salesforce.com and business intelligence (BI) tools for the purpose of tracking the impact of sales programs
* Experience managing or designing sales programs, campaigns, and go-to-market strategies
* Exceptional communication and presentation skills (including with senior management stakeholders)

### Senior Revenue Programs Specialist
The Senior Revenue Programs Specialist reports to the Manager, Revenue Programs.

#### Senior Revenue Programs Specialist Job Grade
The Senior Revenue Programs Specialist role is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Revenue Programs Specialist Responsibilities
* Identify and prioritize sales programs that drive quality pipeline and ARR aligned to GitLab’s go to market (GTM) motions
* Work comfortably with complex data sets to identify pipeline gaps and prescribe and prioritize solutions to meet business pipeline gaps
* Conduct regular program reviews with leaders, providing status updates and risk mitigation strategies
* Work cross-functionally (with Portfolio Marketing, Demand Gen, Revenue Marketing, Field Enablement, Sales Strategy, and more) to orchestrate the development, execution, and operationalization of scalable and measurable quarterly sales programs that deliver results
* Serve as a trusted advisor and business partner to Sales leaders to identify and prioritize sales programs that drive quality pipeline and ARR aligned to GitLab’s go to market (GTM) motions

#### Senior Revenue Programs Specialist Requirements
* Proven ability to design and operationalize prescriptive sales programs and strategies
* Proven track record of cross-stakeholder program management and execution with operational rigor
* Ability to move fluidly from big picture to details
* Ability to work collaboratively with Sales & Marketing teams and effectively develop relationships with stakeholders at all levels of the organization to remove impediments that block the team’s ability to meet project objectives
* Over achievement in a quota-carrying sales role strongly preferred

### Manager, Revenue Programs
The Manager, Revenue Programs reports to the Senior Director of Field Enablement.

#### Manager, Revenue Programs Job Grade
The Manager, Revenue Programs role is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager, Revenue Programs Team Responsibilities
* Lead a team of Revenue Program Specialists

#### Manager, Revenue Programs Team Requirements
* People management experience, preferably in a Sales, Sales Ops, Sales Programs, Sales Analytics, Sales Enablement, or related function
* Ability to influence others

## Performance Indicators 
- [Sales pipeline coverage](/handbook/sales/performance-indicators/#sales-pipeline-coverage)
- [New logos](/handbook/sales/performance-indicators/#new-logos)
* Lead conversion rates with initial emphasis on conversion metrics italicized below
   * _MQL (Marketing Qualified Lead) to SAO (Sales Accepted Opportunity)_
   * _1-Discovery to 2-Scoping_
   * _2-Scoping to 3-Technical Evaluation_
   * 3-Technical Evaluation to 4-Proposal
   * 4-Proposal to 5-Negotiating
   * 5-Negotiating to 6-Awaiting Signature
   * 6-Awaiting Signature to Closed Won

## Career Ladder
The next steps for the Revenue Programs Specialist is to move up within this job family, into the [Field Enablement Leadership](/job-families/sales/director-of-field-enablement/) job family, or into a Sales, Customer Success, or Marketing role.

## Hiring Process
Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

- Selected candidates will be invited to schedule a 30min [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters
- Next, candidates will be invited to schedule a first interview with the Hiring Manager
- Next, candidates will be invited to interview with 2-5 team members
- There may be a final executive interview 
