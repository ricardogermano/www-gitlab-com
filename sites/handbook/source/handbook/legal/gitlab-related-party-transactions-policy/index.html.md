---
layout: handbook-page-toc
title: "GitLab Related Party Transactions Policy"
description: "Overview of the Related Party Transactions Policy at GitLab."
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# GITLAB INC. RELATED PARTY TRANSACTIONS POLICY

## PURPOSE
* This policy requires that each Related Party Transaction (as defined below) be considered for approval or ratification in accordance with the guidelines set forth in this policy (i) by the Audit Committee (the “Audit Committee”) of the Board of Directors (the “Board”) of GitLab Inc. (the “Company”) or (ii) if the Audit Committee determines that the approval or ratification of such Related Party Transaction should be considered by all of the disinterested members of the Board, by such disinterested members of the Board by the vote of a majority thereof.
* A Related Party Transaction will be entered into or continued only if the Audit Committee approves or ratifies the transaction in accordance with the guidelines set out in this policy having determined that the transaction is on terms that are not inconsistent with the best interest of the Company and its shareholders, or if the transaction is approved by the disinterested members of the Board. This policy applies to all Related Party Transactions.
* The Audit Committee may review this policy from time to time and recommend amendments for consideration and approval by the Board.

## DEFINITIONS
* “Related Party Transaction”  means any financial transaction, arrangement or relationship or series of similar transactions, arrangements or relationships (including indebtedness or guarantee of indebtedness) in which:

1. the aggregate amount involved will or may be expected to exceed $120,000 (USD) in any fiscal year   (including   any periodic payments or installments due on or after the beginning of the applicable   fiscal   year   and,   in   the case of indebtedness, the largest amount expected to be outstanding and the amount of annual interest thereon),
1. the Company or any of its subsidiaries is a participant, and
1. any Related Party has or will have a direct or indirect interest.

* “Related Party" means:

1. any person who is or at anytime since the beginning of the Company’s last fiscal year was a director or executive officer of the Company;
1. any shareholder who beneficially owns in excess of 5% of the Company’s outstanding common stock;
1. a person who is an immediate family member of any director or executive officer(which means any child, stepchild, parent, stepparent, spouse, sibling, mother-in-law, father-in-law, son-in-law, daughter-in-law, brother-in-law, sister-in law, and any person other than a tenant or employee sharing the house of such director or executive officer); and
1. any firm, corporation, charitable organization or other entity in which any of these persons is employed or an officer, general partner or principal or in a similar position or in which the person and all related parties has beneficial ownership interest of 10% or more.

## APPROVAL PROCESS
Prior to entering into the Related Party Transaction, the Related Party (or if the Related Party has an immediate family member who is a Related Party) must provide written notice to the Corporate Secretary of the facts and circumstances of the proposed Related Party Transaction. The written notice should include:

1. the Related Party’s relationship to the Company and the person’s interest in the transaction;
1. the   material   terms   of   the   proposed   transaction,   including   the   aggregate   value   or,   in   the   case   of in debtedness, the aggregate principal and interest rate;
1. the benefits to the Company of the proposed transaction;
1. if applicable, the availability of other sources of comparable products or services; and
1. as assessment of whether the proposed transaction is on terms that are comparable to the terms available to an unrelated third party.

The  Corporate  Secretary  will  determine  whether  the  proposed  transaction is a Related  Party Transaction  for purposes of this policy and may meet with the relevant business unit or function leader to confirm and supplement the information in the notice. Any proposed transaction determined to be a Related Party Transaction will be submitted   to   the   disinterested   members   of   the   Audit   Committee   for   consideration   at   its next meeting. If the Corporate Secretary, in consultation with the Chief Executive Officer or the Chief Financial Officer, determines that it is not practical for the Company to wait until the next Audit Committee meeting, the Chair of the Audit Committee has the authority to act between Committee meetings unless the Chair of the Audit Committee is a Related Party in the Related Party Transaction.

The Audit Committee will consider all the relevant facts and circumstances, including the benefits to the Company, the potential effect on a director’s independence of entering into the transaction, the availability of other sources for the products or services, the terms of the transaction and the terms available to unrelated third parties generally. The Audit Committee may approve Related Party Transactions that it determines in good faith are not in consistent with the best interests of the Company and its shareholders. The Chair of the Audit Committee will report to the Audit Committee at its next meeting with regard to any approval of a proposed transaction between Committee meetings under this policy. In the event multiple members of the Audit Committee, including the Chair of the Audit Committee, are Related Parties, the Related Party Transaction will be considered by the disinterested members of the Board in place of the Audit Committee.

## RATIFICATION
If the Company or a Related Party becomes aware that any Related Party Transaction exists that has not been previously approved or ratified under this policy, it will promptly submit the transaction to the Audit Committee or Chair of the Committee or disinterested members of the Board for consideration.

If the Company enters into a transaction that (i) the Company was not aware a Related Party Transaction at the time   it was entered into but which it subsequently determines is a Related Party Transaction   prior to full performance there of or (ii) did not constitute a Related Party Transaction at the time such transaction was entered into but thereafter becomes a Related Party Transaction prior to full performance thereof, then in either such case the Related Party Transaction shall be presented for consideration in the manner set forth above. The Audit Committee or Chair of the Committee or Board will evaluate the transaction considering the criteria set out in the approval process under this policy and will consider all options, including ratification, amendment or termination of the Related Party Transaction.

## REVIEW OF ONGOING TRANSACTIONS
At the Audit Committee’s first meeting of each fiscal year, the committee will evaluate any continuing Related Party Transactions that have remaining amounts receivable of more than $120,000 to determine if it is in the best   interests of the Company and its shareholders to continue, modify or terminate the Related Party Transaction.

## DISCLOSURE
All Related Party Transactions will be disclosed to the Audit Committee and any material Related Party Transaction will be disclosed to the Board.
